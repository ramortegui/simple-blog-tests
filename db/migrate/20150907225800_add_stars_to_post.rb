class AddStarsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :stars, :int, :default => 0
  end
end
