class FixFeaturedOnPosts < ActiveRecord::Migration
  def change
    rename_column :posts, :featured_posts, :featured
  end
end
