class AddFeaturedPostToPost < ActiveRecord::Migration
  def change
    add_column :posts, :featured_posts, :boolean
  end
end
