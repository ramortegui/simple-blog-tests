class Post < ActiveRecord::Base
  belongs_to :user
  validates :content, presence: true, allow_blank: false
  validates :user_id, presence: true

  validate do |post|
    WordValidator.new(post).validate
  end

end

class WordValidator
  def initialize(post)
    @post = post
    @error = ""
  end

  def validate
    words = @post.content.split(/ /)
    words.each do |word|
      if !(Word.where(word: word.downcase.gsub(/[^a-z ]/, '')).count > 0)
        @error += " '#{word}'"
        suggestion = ""
        wt = word

        ind = 1
        while (wt.length > 1 && ind <= wt.length) do
          finder =  wt.slice((-1 * ind)..-1)
          if Word.where(word: finder).count > 0
            suggestion = " #{finder}"+suggestion
            wt.slice!((-1 * ind)..-1)
            ind = 1
            next
          end
          ind += 1
        end
        if suggestion != ""
         @error+= " (try splitting into:#{suggestion})"
         suggestion = ""
        end
      end
    end
    if @error != "" 
      @post.errors[:content] = "Not found:"+@error
    end
  end
end
