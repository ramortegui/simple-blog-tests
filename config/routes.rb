Rails.application.routes.draw do
  resources :posts do
    member do
      post 'add_star'
    end
  end

  root to: 'visitors#index'
  devise_for :users
  resources :users
end
